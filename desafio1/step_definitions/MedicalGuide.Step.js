const { I, HomePage, SearchPage, SearchResult } = inject();

Given("Que acesse a guia médica", () => {
	I.amOnPage("/");
	I.seeInTitle("Portal Nacional de Saúde - Unimed - Institucional");
	HomePage.acessMedicalGuide(I);
});

When("Pesquisar por {string}", (contetSearch) => {
	I.amOnPage("/guia-medico#/");
	SearchPage.searchFor(I, contetSearch);
});

When("Clicar em Ver mais resultados", async () => {
	await SearchResult.seeMoreResults(I, 2);
});

Then("Verei nos resultados somente cidades do Rio de Janeiro", async () => {
	var adressElements = await SearchResult.checkAdressInfo(I);
	I.assertEach(adressElements, (adressElements) =>
		adressElements.includes("Rio de Janeiro")
	);
});

Then("Verei nos resultados as especialidades atendidas", async () => {
	var specialityElements = await SearchResult.checkSpecialtyInfo(I);
	I.assertEach(
		specialityElements,
		(specialityElements) => typeof specialityElements == "string"
	);
});
