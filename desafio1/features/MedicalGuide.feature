Feature: Desafio 1
Pesquisar na Guia Médica por médicos no Rio de Janeiro

@desafio1part1
  Scenario: Validar as cidades nos resultados
    Given Que acesse a guia médica
    When Pesquisar por "Médicos no Rio de Janeiro"
    Then Verei nos resultados somente cidades do Rio de Janeiro

@desafio1part1
  Scenario: Validar as especialidades resultados
    Given Que acesse a guia médica
    When Pesquisar por "Médicos no Rio de Janeiro"
    Then Verei nos resultados as especialidades atendidas

@desafio1part2
  Scenario: Validar as especialidades resultados
    Given Que acesse a guia médica
    When Pesquisar por "Médicos no Rio de Janeiro"
    And Clicar em Ver mais resultados
    Then Verei nos resultados somente cidades do Rio de Janeiro