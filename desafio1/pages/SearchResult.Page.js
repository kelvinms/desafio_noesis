class SearchResult {
	constructor() {
		this.locators = {
			label: {
				speciality: { xpath: "//span[@class='ProviderSpecialties--item']" },
			},
			resultGrid: {
				gridBoxs: { xpath: "//*[@class='ProviderCard']" },
				gridLoader: { xpath: "//*[@class='ProviderLoader']" },
			},
			buttons: {
				seeMore: {
					xpath: "//*[contains(text(),'Ver mais resultados')]",
				},
			},
		};
	}

	async checkAdressInfo(I) {
		const adressElements = await I.grabHTMLFromAll(
			"//a[@class='ProviderAddressAsGrid--address-link']"
		);
		return adressElements;
	}

	async checkSpecialtyInfo(I) {
		const specialityElements = await I.grabHTMLFromAll(
			this.locators.label.speciality
		);
		return specialityElements;
	}

	async seeMoreResults(I, quantity) {
		for (quantity; quantity > 0; quantity--) {
			I.scrollTo(this.locators.buttons.seeMore);
			I.click(this.locators.buttons.seeMore);
			I.waitForInvisible(this.locators.resultGrid.gridLoader, 10);
		}
	}
}
module.exports = new SearchResult();
