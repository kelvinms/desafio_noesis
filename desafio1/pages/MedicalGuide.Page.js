const { SearchResult } = inject();

class MedicalGuide {
	constructor() {
		this.locators = {
			buttons: {
				search: {
					xpath: "//button[contains(text(),'Pesquisar')]",
				},
				providerProfile: {
					xpath: "//button[contains(text(),'Perfil do Prestador')]",
				},
			},
			fields: {
				searchBar: { react: "t", state: { inputId: "react-select-2-input" } },
			},
		};
	}

	async searchFor(I, content) {
		I.see("Busca rápida");
		I.click({ id: "acceptCookie" });
		I.seeElement(this.locators.fields.searchBar);
		I.wait(1);
		I.fillField("//*[@class='AutoSuggest__input']/input", content);
		I.wait(1);
		I.seeElement(this.locators.buttons.search);
		I.click(this.locators.buttons.search);
		I.waitForInvisible(SearchResult.locators.resultGrid.gridLoader, 10);
	}
}
module.exports = new MedicalGuide();
