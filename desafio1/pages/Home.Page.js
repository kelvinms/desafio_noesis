class Home {
	constructor() {
		this.locators = {
			buttons: {
				medicalGuide: { xpath: "//*[contains(text(),'Guia Médico')]" },
			},
		};
	}

	async acessMedicalGuide(I) {
		I.click(this.locators.buttons.medicalGuide);
	}
}
module.exports = new Home();
