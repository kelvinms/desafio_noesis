exports.config = {
	output: "./output",
	helpers: {
		WebDriver: {
			url: "https://www.unimed.coop.br/",
			browser: "chrome",
			windowSize: "maximize",
			restart: true,
		},
		AssertWrapper: {
			require: "codeceptjs-assert",
		},
	},
	include: {
		HomePage: "./pages/Home.Page.js",
		SearchPage: "./pages/MedicalGuide.Page.js",
		SearchResult: "./pages/SearchResult.Page.js",
	},
	mocha: {},
	bootstrap: null,
	teardown: null,
	hooks: [],
	gherkin: {
		features: "./features/*.feature",
		steps: ["./step_definitions/MedicalGuide.Step.js"],
	},
	plugins: {
		allure: {
			enabled: true,
			outputDir: "./output",
		},
		wdio: {
			enabled: true,
			services: ["selenium-standalone"],
		},
		pauseOnFail: {},
		retryFailedStep: {
			enabled: true,
		},
		tryTo: {
			enabled: true,
		},
	},
	tests: "./*_test.js",
	name: "desafionoesis",
};
