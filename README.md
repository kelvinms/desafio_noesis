# technical Challenge Noesis

> Challenge 1

```
java -version
java version "1.8.0_241"
Java(TM) SE Runtime Environment (build 1.8.0_241-b07)
Java HotSpot(TM) 64-Bit Server VM (build 25.241-b07, mixed mode)

Windows 10 Home
10.0.19042

node -v
v12.18.3

npm -v
6.14.6
```

### Installation

OS X, Linux or Windows with npm:

```
Clone project
git clone https://gitlab.com/kelvinms/desafio_noesis

Navegate to folder
cd desafio1

Install all dependencies
npm install
```

### To test

```
- To all run tests atutomatically
`npx codeceptjs run`

- To see the Report Results
`allure serve output`
O relatório ficará disponivel na URL http://127.0.1.1:36125/index.html

```

> Challenge 2

```
Tested with Jmeter
apache-jmeter-5.4.1
```
